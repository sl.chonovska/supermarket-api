# Supermarket API

## This project creates the back-end of an imaginary Supermarket. 
### The goal is to have the following functionality:
- register and login users
- create/edit/delete products
- keep an inventory
- mark products to be on sale
- order products

## How to setup the initial project:
1. Pip install the requirements.txt file
2. Copy the .env.example to make your own .env file
3. Fill the new .env file with the neccessary information to connect to the local database
4. In the command line run the command 'python manage.py makemigrations' and then the command 'python manage.py migrate' to run all the migrations to the database. You can make a new migration by editing any of the models.py files and running the two commands again.
5. You can run the server by typing the command 'python manage.py runserver'
   