from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


# upload image to the appropriate folder
def upload_to(instance, filename):
    return "images/{filename}".format(filename=filename)


class User(AbstractUser):
    email = models.EmailField(max_length=254, blank=False, unique=True)
    avatar = models.ImageField(upload_to=upload_to, blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True)
    is_authenticated = models.BooleanField(default=False)


class Address(models.Model):
    address = models.CharField(max_length=254, blank=False, null=False)
    city = models.CharField(max_length=100, blank=False, null=False)
    country = models.CharField(max_length=100, blank=False, null=False)
    owner = models.ForeignKey("User", related_name="addresses", on_delete=models.CASCADE)
