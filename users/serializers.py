from rest_framework import serializers
from users.models import User, Address
from supermarket import settings
from django.core.mail import send_mail

class AddressSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Address
        fields = ["url", "id", "address", "city", "country", "owner"]


class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "username",
            "password",
        ]
        extra_kwargs = {"password": {"write_only": True}}

class UserSerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "avatar",
            "password",
            "addresses",
        ]
        extra_kwargs = {"password": {"write_only": True}, "id": {"read_only": True}}

    # change password to be hashed
    def create(self, validated_data):
        #hashing the password
        password = validated_data.pop("password", None)
        instance = self.Meta.model(**validated_data)
        if password:
            instance.set_password(password)

        # send an email for verification
        email = validated_data.get("email")
        verification_link = f"localhost:8000/auth/verify-account/{validated_data.get("username")}"
        subject = "Verify account"
        body = f"Please click on this link to verify your email: <a href={verification_link}>Click me to verify</a>"
        from_email = settings.DEFAULT_FROM_EMAIL
        send_mail(subject=subject, html_message=body, from_email=from_email, recipient_list=[email], fail_silently=False)


        instance.save()
        return instance
