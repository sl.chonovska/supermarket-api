from django.urls import path
from users.views import RegisterView, UserView, LogoutView, LoginView, verify_account

urlpatterns = [
    path("register", RegisterView.as_view()),
    path("login/", LoginView.as_view(), name="login"),
    path("user", UserView.as_view()),
    path("logout", LogoutView.as_view()),
    path("verify-account/<str:username>", verify_account),
]
