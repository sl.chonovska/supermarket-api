from django.contrib.auth.signals import user_logged_in
from django.middleware import csrf
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from users.serializers import UserSerializer, AddressSerializer, UserLoginSerializer
from supermarket.permissions import IsOwner
from users.models import User
from supermarket import settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.exceptions import TokenError
from drf_yasg.utils import swagger_auto_schema
from drf_yasg.openapi import Schema


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }


@api_view(("GET",))
def verify_account(request, username):
    """
    View function to verify a user's account.
    """
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return Response(data="No such user!", status=status.HTTP_404_NOT_FOUND)

    if user.is_authenticated:
        return Response(data="You are already verified!", status=status.HTTP_400_BAD_REQUEST)

    user.is_authenticated = True
    user.save()
    return Response(data=f"{username} you are now verified.", status=status.HTTP_200_OK)


# Create your views here.
class RegisterView(APIView):
    """
    View class to handle user registration.
    """

    @swagger_auto_schema(request_body=UserSerializer)
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED, data={"message": "You are registered, please verify your email."}
        )


class LoginView(APIView):
    """
    View class to handle user login.
    """

    @swagger_auto_schema(request_body=UserLoginSerializer)
    def post(self, request):
        data = request.data
        response = Response()
        username = data.get("username", None)
        password = data.get("password", None)
        user = authenticate(username=username, password=password)

        if not user:
            return Response({"Invalid": "Invalid username or password!!"}, status=status.HTTP_400_BAD_REQUEST)

        if not user.is_active:
            return Response({"No active": "This account is not active!!"}, status=status.HTTP_400_BAD_REQUEST)

        if not user.is_authenticated:
            return Response(
                {"Not authenticated": "This account is not authenticated!!"}, status=status.HTTP_400_BAD_REQUEST
            )

        data = get_tokens_for_user(user)
        response.set_cookie(
            key=settings.SIMPLE_JWT["AUTH_COOKIE"],
            value=data["access"],
            expires=settings.SIMPLE_JWT["ACCESS_TOKEN_LIFETIME"],
            secure=settings.SIMPLE_JWT["AUTH_COOKIE_SECURE"],
            httponly=settings.SIMPLE_JWT["AUTH_COOKIE_HTTP_ONLY"],
            samesite=settings.SIMPLE_JWT["AUTH_COOKIE_SAMESITE"],
        )
        response["X-CSRFToken"] = csrf.get_token(request)
        response.data = {"Success": "Login successfully", "data": data}
        user_logged_in.send(sender=user.__class__, request=request, user=user)
        return response


class UserView(APIView):
    """
    View class to handle user details.
    """

    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(responses={200: UserSerializer(many=False)})
    def get(self, request):
        print("User: ", request.user)
        serializer = UserSerializer(request.user, context={"request": request})
        return Response(serializer.data)

    @swagger_auto_schema(request_body=UserSerializer)
    def put(self, request):
        user = request.user
        serializer = UserSerializer(user, data=request.data, partial=True, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class LogoutView(APIView):
    """
    View class to handle user logout.
    """

    def post(self, request):
        try:
            response = Response()
            response.delete_cookie("access_token")
            response.delete_cookie("csrftoken")
            response.status_code = 204
            response.context = {"request": request}
            response.data = {"msg": "Logout sucess!"}
        except TokenError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={f"Error message: {e}"})

        return response


class AddressViewSet(viewsets.ModelViewSet):
    """
    View class to handle user addresses.
    """

    serializer_class = AddressSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        """
        Restricts the returned address to a given user
        """
        user = self.request.user
        return user.addresses.all()

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(owner=user)
