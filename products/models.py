from django.db import models

# Create your models here.

SALE_EXPIRATION_TYPE_CHOICES = {
    "TIME": "Until Time Period",
    "BOTH": "While Supplies Last or Until Time Period",
    "SUPPLIES": "While Supplies Last",
}

SALE_CHOICES = {
    "PERCENTAGE": "Percent Discount",
    "AMOUNT": "Amount Discount",
    "BUY2GET3": "Buy 2 Get 3",
}


class Category(models.Model):
    label = models.CharField(max_length=50)


class Subcategory(models.Model):
    label = models.CharField(max_length=50)
    category = models.ForeignKey("Category", related_name="subcategory", on_delete=models.CASCADE)


class SaleExpirationType(models.Model):
    expiration_type_name = models.CharField(
        max_length=50, choices=SALE_EXPIRATION_TYPE_CHOICES, blank=False, null=False
    )
    expiration_time_period = models.DateTimeField(blank=True, null=True)


class Sale(models.Model):
    sale_type = models.CharField(max_length=50, choices=SALE_CHOICES, blank=False, null=False)
    expiration_type = models.ForeignKey("SaleExpirationType", related_name="sale_expiration", on_delete=models.CASCADE)
    discount_amount = models.IntegerField(blank=True, null=True)
    discount_percent = models.IntegerField(blank=True, null=True)
    is_running = models.BooleanField(default=False)


class Product(models.Model):
    product_name = models.CharField(max_length=150, blank=False, null=False)
    description = models.CharField(max_length=254, blank=False, null=False)
    brand_name = models.CharField(max_length=50, blank=False, null=False)
    price = models.DecimalField(decimal_places=2, max_digits=10, blank=False, null=False)
    quantity = models.IntegerField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    is_hot_product = models.BooleanField(default=False)
    created_by = models.ForeignKey("users.User", related_name="products", on_delete=models.SET_NULL, null=True)
    subcategory = models.ForeignKey("Subcategory", related_name="related_product", on_delete=models.SET_NULL, null=True)
    sale = models.ForeignKey("Sale", related_name="product_on_sale", on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["id"]


class Favourite(models.Model):
    user = models.ForeignKey("users.User", related_name="favourites", on_delete=models.CASCADE, null=False, blank=False)
    product = models.ForeignKey("Product", related_name="likes", on_delete=models.CASCADE, null=False, blank=False)
