import datetime
from rest_framework import serializers
from products import models


class CategorySerilializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = "__all__"


class SubcategorySerializer(serializers.ModelSerializer):
    category = CategorySerilializer(read_only=True)

    class Meta:
        model = models.Subcategory
        fields = ["id", "label", "category"]


class SaleExpirationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SaleExpirationType
        fields = "__all__"
        read_only_fields = ("id",)

    def validate(self, data):
        """
        Check that the type and date are valid.
        """

        if data["expiration_type_name"] == "SUPPLIES" and "expiration_time_period" in data.keys():
            raise serializers.ValidationError("This type of sale expiration cannot have a set time period!")

        if data["expiration_type_name"] in ("TIME", "BOTH") and "expiration_time_period" not in data.keys():
            raise serializers.ValidationError("This type of sale expiration must have a set time period!")
        elif data["expiration_type_name"] != "SUPPLIES" and data["expiration_time_period"] < datetime.datetime.now(
            datetime.timezone.utc
        ):
            raise serializers.ValidationError("Date must be a future date!")
        return data


class SaleSerializer(serializers.ModelSerializer):
    expiration_type = SaleExpirationSerializer(read_only=True)

    class Meta:
        model = models.Sale
        fields = ["id", "sale_type", "discount_amount", "discount_percent", "is_running", "expiration_type"]
        read_only_fields = ("id", "is_running")

    def validate(self, data):
        """
        Check that the type and date are valid.
        """
        if data["sale_type"] == "PERCENTAGE" and (
            "discount_percent" not in data.keys() or "discount_amount" in data.keys()
        ):
            raise serializers.ValidationError("This type of sale needs only a discount percentage!")
        if data["sale_type"] == "AMOUNT" and (
            "discount_percent" in data.keys() or "discount_amount" not in data.keys()
        ):
            raise serializers.ValidationError("This type of sale needs only a discount amount!")
        if data["sale_type"] == "BUY2GET3" and ("discount_percent", "discount_amount") in data.keys():
            raise serializers.ValidationError("This type of sale does not take any numerical discounts!")
        return data


class ProductSerializer(serializers.ModelSerializer):
    subcategory = SubcategorySerializer(read_only=True)
    created_by = serializers.ReadOnlyField(source="created_by.username")
    sale = SaleSerializer(read_only=True)

    class Meta:
        model = models.Product
        fields = [
            "id",
            "product_name",
            "description",
            "brand_name",
            "price",
            "quantity",
            "created_by",
            "created_at",
            "updated_at",
            "is_hot_product",
            "subcategory",
            "sale",
        ]
        read_only_fields = ("id", "created_at", "updated_at", "is_hot_product")

    def validate_quantity(self, value):
        if value < 0:
            raise serializers.ValidationError("Quantity cannot be less than 0!")
        return value

    def validate_price(self, value):
        if value <= 0:
            raise serializers.ValidationError("Price cannot be less than or equal to 0!")
        return value
