from datetime import datetime
from django.utils.timezone import make_aware
from django.core.mail import send_mail
from supermarket import settings
from supermarket.permissions import IsStaffOrReadOnly
from rest_framework.permissions import IsAdminUser
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ParseError
from rest_framework.decorators import action
from products.models import Product, Subcategory, Favourite, SaleExpirationType, Sale
from users.models import User
from products.serializers import ProductSerializer, SaleExpirationSerializer, SaleSerializer
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema


# Create your views here.


class SaleExpirationViewSet(viewsets.ModelViewSet):
    queryset = SaleExpirationType.objects.all()
    serializer_class = SaleExpirationSerializer
    permission_classes = [IsAdminUser]

    def partial_update(self, request, *args, **kwargs):
        # make sure the type is always passed for validation
        instance = self.get_object()
        request.data["expiration_type_name"] = (
            instance.expiration_type_name if instance.expiration_type_name else request.data["expiration_type_name"]
        )
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


def validate_sale_expiration_type(id):
    try:
        return SaleExpirationType.objects.get(id=id)
    except SaleExpirationType.DoesNotExist:
        raise NotFound(detail="Sale expiration not found!", code=404)


class SaleViewSet(viewsets.ModelViewSet):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer
    permission_classes = [IsAdminUser]

    def perform_create(self, serializer):
        try:
            expiration_type_obj = validate_sale_expiration_type(self.request.data["expiration_type"])
        except KeyError:
            raise ParseError(detail="Please provide a sale expiration!", code=400)
        serializer.save(expiration_type=expiration_type_obj)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        request.data["sale_type"] = instance.sale_type if instance.sale_type else request.data["sale_type"]
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        if "expiration_type" in self.request.data.keys():
            expiration_type_obj = validate_sale_expiration_type(self.request.data["expiration_type"])
            serializer.save(expiration_type=expiration_type_obj)
        serializer.save()
        return Response(serializer.data)

    @action(detail=True, methods=["POST"], url_name="toggle-is-running", url_path="toggle-is-running")
    def sale_toggle_is_running(self, request, pk=None):
        # Toggle the if the sale is running or not
        try:
            sale = self.get_object()
        except Sale.DoesNotExist:
            raise NotFound(detail="Sale not found!", code=404)
        sale.is_running = not sale.is_running
        sale.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = [DjangoFilterBackend]

    def perform_create(self, serializer):
        user = self.request.user
        try:
            subcategory = Subcategory.objects.get(label=self.request.data["subcategory"])
        except Subcategory.DoesNotExist:
            raise NotFound(detail="Subcategory not found!", code=404)
        except KeyError:
            raise ParseError(detail="Please provide a subcategory!", code=400)
        serializer.save(created_by=user, subcategory=subcategory)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        if "quantity" in self.request.data.keys():
            if instance.quantity == 0 and request.data["quantity"] > 0:
                # send an email to users who favourited the restocked product
                users = Favourite.objects.filter(product=instance).values_list("user", flat=True)
                user_emails = User.objects.filter(pk__in=users).values_list("email", flat=True)
                subject = "Favourite product is restocked."
                body = f"The product: {instance.product_name}, you favourited has been restocked."
                from_email = settings.DEFAULT_FROM_EMAIL
                send_mail(
                    subject=subject,
                    message=body,
                    from_email=from_email,
                    recipient_list=list(user_emails),
                    fail_silently=False,
                )
        if "subcategory" in self.request.data.keys():
            try:
                subcategory = Subcategory.objects.get(label=self.request.data["subcategory"])
                serializer.save(subcategory=subcategory)
            except Subcategory.DoesNotExist:
                raise NotFound(detail="Subcategory not found!", code=404)

        serializer.save(updated_at=make_aware(datetime.utcnow()))
        return Response(serializer.data)

    # <-------------------------HOT PRODUCT FUNTIONALITY------------------------------------------------------>

    @action(detail=True, methods=["POST"], url_name="toggle-hot", url_path="toggle-hot")
    @swagger_auto_schema(request_body=None)
    def toggle_hot(self, request, pk=None):
        # Toggle the if the product is hot or not
        try:
            product = self.get_object()
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!", code=404)
        product.is_hot_product = not product.is_hot_product
        product.save()
        product.updated_at = make_aware(datetime.utcnow())
        product.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=["GET"], detail=False, url_name="hot-products", url_path="hot-products")
    def hot_products(self, request):
        # Get all products marked as hot
        hot_products = self.queryset.filter(is_hot_product=True)
        serializer = self.get_serializer(hot_products, many=True)
        return Response(serializer.data)

    # <-------------------------FAVOURITE FUNTIONALITY------------------------------------------------------>

    @action(
        detail=True,
        methods=["POST"],
        url_name="favourite",
        url_path="favourite",
        permission_classes=[IsAuthenticated],
    )
    @swagger_auto_schema(request_body=None)
    def mark_favourite(self, request, pk=None):
        # Mark product as a favourite
        try:
            product = self.get_object()
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!", code=404)
        if Favourite.objects.filter(user=request.user, product=product):
            raise ParseError(detail="This product is already favourited!", code=400)
        favourite = Favourite(user=request.user, product=product)
        favourite.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=["GET"], detail=False, url_name="favourite", url_path="favourite")
    def favourite_products(self, request):
        # Get all products marked as favourites

        # get the product ids of the favourite products of the user
        favourite_products_pks = Favourite.objects.filter(user=request.user).values_list("product", flat=True)
        favourite_products = Product.objects.filter(pk__in=favourite_products_pks).all()
        serializer = self.get_serializer(favourite_products, many=True)
        return Response(serializer.data)

    # <-------------------------SIMILIAR PRODUCT FUNTIONALITY------------------------------------------------------>

    @action(detail=True, methods=["GET"], url_name="similiar-products", url_path="similiar-products")
    def similiar_products(self, request, pk=None):
        # Get a list of similiar products (by subcategory)
        try:
            product = self.get_object()
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!", code=404)
        subcategory = product.subcategory
        similiar_products_lst = Product.objects.filter(subcategory=subcategory).exclude(id=product.id).all()
        serializer = ProductSerializer(similiar_products_lst, many=True)
        return Response(serializer.data)

    # <-------------------------SALE PRODUCT FUNTIONALITY------------------------------------------------------>
    @action(
        detail=True,
        methods=["POST"],
        url_name="sale",
        url_path=r"sale/(?P<sale_id>\d+)",
    )
    @swagger_auto_schema(request_body=None)
    def put_product_on_sale(self, request, pk=None, sale_id=None):
        # Put product on sale
        try:
            product = self.get_object()
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!", code=404)
        try:
            sale = Sale.objects.get(id=sale_id)
        except Sale.DoesNotExist:
            raise NotFound(detail="Sale not found!", code=404)
        product.sale = sale
        product.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(
        detail=True,
        methods=["POST"],
        url_name="not on sale",
        url_path=r"discard-sale",
    )
    @swagger_auto_schema(request_body=None)
    def put_product_not_on_sale(self, request, pk=None):
        # Put product not on sale
        try:
            product = self.get_object()
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!", code=404)
        if not product.sale:
            raise ParseError(detail="This product is not on sale!", code=400)
        product.sale = None
        product.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
