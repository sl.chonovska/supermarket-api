from rest_framework import permissions


class IsStaffOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow staff or admin edit object.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the staff.
        return request.user.is_staff or request.user.is_superuser


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only owner to view.
    """

    def has_object_permission(self, request, view, obj):
        # Allow only for the certain user.
        return obj.owner == request.user
