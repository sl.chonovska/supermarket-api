from django.urls import path, include
from orders.views import ShoppingCartViewSet, ShoppingCartEntryViewSet, OrderViewSet
from rest_framework.routers import DefaultRouter

order_router = DefaultRouter()
order_router.register(r"cart-entry", ShoppingCartEntryViewSet, basename="cart-entry")
order_router.register(r"order", OrderViewSet, basename="order")

shopping_cart_price_details = ShoppingCartViewSet.as_view({"get": "total_price_with_discount"})
shopping_cart_detail = ShoppingCartViewSet.as_view({"get": "retrieve"})


urlpatterns = [
    path("cart/", shopping_cart_detail),
    path("cart/total-price/", shopping_cart_price_details),
    path("", include(order_router.urls)),
]
