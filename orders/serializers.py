import datetime
from rest_framework import serializers
from products.serializers import ProductSerializer
from orders import models


class ShoppingCartEntrySerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)
    cart = serializers.ReadOnlyField(source="cart.id")

    class Meta:
        model = models.ShoppingCartEntry
        fields = ["id", "product", "quantity", "cart", "added_at"]


class ShoppingCartSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.username")
    cart_entries = ShoppingCartEntrySerializer(many=True, read_only=True)

    class Meta:
        model = models.ShoppingCart
        fields = ["id", "user", "updated_at", "is_complete", "cart_entries"]
        read_only_fields = ("id", "updated_at", "is_complete")


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Payment
        fields = "__all__"


class OrderSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.username")
    cart = ShoppingCartSerializer(read_only=True)
    payment = PaymentSerializer(read_only=True)

    class Meta:
        model = models.Order
        fields = [
            "id",
            "user",
            "payment",
            "ordered_at",
            "exp_delivery_date",
            "total_price",
            "discount",
            "status",
            "cart",
        ]
        read_only_fields = (
            "id",
            "ordered_at",
            "status",
            "total_price",
            "discount",
        )

    def validate_exp_delivery_date(self, value):
        if value < datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1):
            raise serializers.ValidationError("Date must be a future date!")
        return value
