from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.

ORDER_STATUS = {"A": "Accepted", "P": "Processing", "OFD": "Out for Delivery", "D": "Delivered", "C": "Cancelled"}


class ShoppingCart(models.Model):
    user = models.ForeignKey("users.User", related_name="carts", on_delete=models.CASCADE, null=False, blank=False)
    updated_at = models.DateTimeField(blank=True, null=True)
    is_complete = models.BooleanField(default=False)


class ShoppingCartEntry(models.Model):
    product = models.ForeignKey(
        "products.Product", related_name="cart_product", on_delete=models.CASCADE, null=False, blank=False
    )
    quantity = models.IntegerField(validators=[MinValueValidator(1)], blank=False, null=False)
    added_at = models.DateTimeField(auto_now_add=True)
    cart = models.ForeignKey(
        "ShoppingCart", related_name="cart_entries", on_delete=models.CASCADE, null=False, blank=False
    )


class Payment(models.Model):
    payment_type = models.CharField(max_length=150, default="Pay on Delivery")
    paid_at = models.DateTimeField(auto_now_add=True)


class Order(models.Model):
    user = models.ForeignKey("users.User", related_name="orders", on_delete=models.CASCADE, null=False, blank=False)
    cart = models.ForeignKey("ShoppingCart", related_name="order", on_delete=models.CASCADE, null=False, blank=False)
    payment = models.ForeignKey("Payment", related_name="paid_order", on_delete=models.CASCADE, null=False, blank=False)
    ordered_at = models.DateTimeField(auto_now_add=True)
    exp_delivery_date = models.DateTimeField(blank=False, null=False)
    status = models.CharField(max_length=5, choices=ORDER_STATUS, blank=False, null=False)
    total_price = models.DecimalField(decimal_places=2, max_digits=5, blank=False, null=False)
    discount = models.DecimalField(decimal_places=2, max_digits=5, blank=True, null=True)
