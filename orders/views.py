from datetime import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.utils.timezone import make_aware
from orders.serializers import ShoppingCartEntrySerializer, ShoppingCartSerializer, OrderSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound, ParseError
from rest_framework import viewsets, mixins
from orders.models import ShoppingCart, Order, Payment, ShoppingCartEntry
from products.models import Product
from users.models import User
from rest_framework.decorators import action
from drf_yasg.utils import swagger_auto_schema
from decimal import Decimal

# Create your views here.


def get_user_shopping_cart(user: User) -> ShoppingCart:
    # gets a user's cart and if no shopping cart create one
    try:
        cart = user.carts.get(is_complete=False)
    except ShoppingCart.DoesNotExist:
        cart = ShoppingCart(user=user)
        cart.save()
    return cart


def shopping_cart_total_price_with_discount(user: User, place_order: bool = False):
    total_price = Decimal(0.0)
    discount = Decimal(0.0)
    # get user cart
    user_cart = get_user_shopping_cart(user)
    # get a list of cart entries
    shopping_cart_entry_lst = ShoppingCartEntry.objects.filter(cart=user_cart).all()
    response = {"cart_id": user_cart.id, "total_price": total_price, "discount": discount}
    # check if shopping cart has product inside
    if not shopping_cart_entry_lst:
        # throw an error if we are trying to place an order
        if place_order:
            detail = "Cannot order with an empty shopping cart!"
            raise ParseError(code=status.HTTP_400_BAD_REQUEST, detail=detail)
        return response

    products_with_discounts_lst = []
    for entry in shopping_cart_entry_lst:
        if entry.product.sale_id:
            # if it has sale add it to the number of times it is ordered
            products_with_discounts_lst.extend([entry.product] * entry.quantity)
        if place_order:
            # subtract ordered quantity from the total product quantity if place_order
            entry.product.quantity -= entry.quantity
            entry.product.updated_at = make_aware(datetime.utcnow())
            entry.product.save()
        # add price of product to total price
        total_price += entry.product.price * entry.quantity
    response["total_price"] = total_price
    if products_with_discounts_lst:
        discount = get_discount(products_with_discounts_lst)
        response["discount"] = discount
    return response


def get_discount(products_with_discounts_lst: list):
    discount = Decimal(0.0)
    buy2get3_products = []

    for product in products_with_discounts_lst:
        # add the correct discount for each type
        if product.sale.sale_type == "BUY2GET3":
            buy2get3_products.append(product)
        if product.sale.sale_type == "AMOUNT":
            discount += product.sale.discount_amount
        if product.sale.sale_type == "PERCENTAGE":
            discount += product.price * Decimal(product.sale.discount_percent / 100)

    # if there are at least 3 products with the sale get 2 buy 3
    if len(buy2get3_products) >= 3:
        # sort them in ascending order by price
        buy2get3_products.sort(key=lambda x: x.price)
        # get how many of them should be free
        num_free_products = len(buy2get3_products) // 3
        free_products = buy2get3_products[:num_free_products]
        # add the price for each free product to the discount
        for product in free_products:
            discount += product.price

    return discount


class ShoppingCartViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    """
    View class to handle user addresses.
    """

    permission_classes = [IsAuthenticated]
    serializer_class = ShoppingCartSerializer

    def retrieve(self, request):
        """
        Restricts the returned cart to a given user
        """
        user = request.user
        cart = get_user_shopping_cart(user)
        serializer = ShoppingCartSerializer(cart)
        return Response(serializer.data)

    @action(methods=["get"], detail=True, url_path="total-price")
    def total_price_with_discount(self, request):
        response = shopping_cart_total_price_with_discount(user=request.user)
        return Response(response)


class ShoppingCartEntryViewSet(viewsets.ModelViewSet):
    serializer_class = ShoppingCartEntrySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """
        Restricts the returned entries to a given user
        """
        user = self.request.user
        cart = get_user_shopping_cart(user)
        return cart.cart_entries

    def perform_create(self, serializer):
        user = self.request.user
        try:
            product_id = self.request.data["product"]
            product = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            raise NotFound(detail="Product not found!")
        except KeyError:
            raise ParseError(detail="Please enter a product!")

        try:
            if product.quantity < self.request.data["quantity"]:
                raise ParseError(detail="There isn't enough stock of this product")
        except KeyError:
            raise ParseError(detail="Please enter quantity!")

        cart = get_user_shopping_cart(user)
        cart.updated_at = make_aware(datetime.utcnow())
        cart.save()
        serializer.save(product=product, cart=cart)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            if instance.product.quantity < self.request.data["quantity"]:
                raise ParseError(detail="There isn't enough stock of this product")
        except KeyError:
            raise ParseError(detail="Please enter quantity!")
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        cart = get_user_shopping_cart(request.user)
        cart.updated_at = make_aware(datetime.utcnow())
        cart.save()
        return Response(serializer.data)


def validate_order_within_two_hours(order: Order):
    # get the difference in seconds
    time_difference_in_seconds = (order.exp_delivery_date - make_aware(datetime.utcnow())).total_seconds()
    # convert seconds to hours and check if within the allowed two hours
    if time_difference_in_seconds // 3600 < 2:
        detail = "Order cannot be canceled 2 hours or less before the delivery time"
        raise ParseError(code=status.HTTP_400_BAD_REQUEST, detail=detail)
    return True


# TODO: calculate order's total price
class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]
    http_method_names = ["get", "post", "head", "patch"]

    def get_queryset(self):
        """
        Restricts the returned entries to a given user
        """
        user = self.request.user
        orders = Order.objects.filter(user=user).all()
        return orders

    # TODO: put order's total price and test
    def perform_create(self, serializer):
        user = self.request.user
        cart = get_user_shopping_cart(user)
        total_price_with_discount = shopping_cart_total_price_with_discount(user, place_order=True)
        payment = Payment()
        payment.save()
        serializer.save(
            user=user,
            cart=cart,
            payment=payment,
            exp_delivery_date=self.request.data["exp_delivery_date"],
            status="P",
            total_price=total_price_with_discount.get("total_price"),
            discount=total_price_with_discount.get("discount"),
        )
        # set the cart to complete
        cart.is_complete = True
        cart.save()

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        if validate_order_within_two_hours(instance) and instance.status != "C":
            serializer = self.get_serializer(instance, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        raise ParseError(detail="Cannot edit this order!")

    @action(detail=True, methods=["PATCH"], url_name="cancel-order", url_path="cancel")
    def cancel_order(self, request, pk=None):
        try:
            order = self.get_object()
        except Order.DoesNotExist:
            raise NotFound(detail="Order not found!", code=404)
        if validate_order_within_two_hours(order) and order.status != "C":
            # get all entries and update the quantity
            entries = ShoppingCartEntry.objects.filter(cart=order.cart).all()
            for entry in entries:
                entry.product.quantity += entry.quantity
                entry.product.updated_at = make_aware(datetime.utcnow())
                entry.product.save()
            order.status = "C"
            order.save()

            return Response(status=status.HTTP_204_NO_CONTENT)
        raise ParseError(detail="This order is already cancelled!")
